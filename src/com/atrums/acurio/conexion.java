package com.atrums.acurio;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

public class conexion {
	private static DataSource dataSource = null;
	
	static {
		BasicDataSource basicDataSource = new BasicDataSource();
		
		try {
			dataSource = basicDataSource;
			
			basicDataSource.setDriverClassName("org.postgresql.Driver");
			basicDataSource.setUsername("postgres");
			basicDataSource.setPassword("AcurioOB+2018");
			basicDataSource.setUrl("jdbc:postgresql://" + "192.168.1.25:5432/openbravo");
			//basicDataSource.setPassword("Acurio2014+");
			//basicDataSource.setUrl("jdbc:postgresql://" + "190.57.150.54:5432/openbravo");
			//basicDataSource.setPassword("Acurio2014+");
			//basicDataSource.setUrl("jdbc:postgresql://" + "190.57.150.53:5432/openbravoproduccion");
			basicDataSource.setMaxActive(20);
			basicDataSource.setMaxIdle(20);
			basicDataSource.setInitialSize(1);
			basicDataSource.setMaxWait(5000);
			basicDataSource.setRemoveAbandonedTimeout(300);
		} catch (Exception ex) {
			System.err.println(ex.getStackTrace());
		}
	}
	
	public static DataSource getDataSource() {
		return dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		conexion.dataSource = dataSource;
	}
}
