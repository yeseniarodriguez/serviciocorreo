package com.atrums.acurio;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.sql.DataSource;

public class controlServicio implements Runnable{

	String correo = null;
    String password = null;
    String banco = null;
    DataSource dataSourceOper = null;
	
    static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private Store store;
	private Session sesion;
    
	public controlServicio(String correo, String password, String banco, DataSource dataSourceOper){
    	this.correo = correo;
    	this.password = password;
    	this.banco = banco;
    	this.dataSourceOper = dataSourceOper;
    }
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		ExecutorService exService = Executors.newFixedThreadPool(1);
		Future<?> future = null;
		Connection connection = null;
		Properties prop = new Properties();
		
		try{
			connection = getConneccion(dataSourceOper);
			
			prop.put("mail.host", "outlook.office365.com");
			prop.put("mail.store.protocol", "imaps");
			prop.put("mail.imap.starttls.enable", "true");
			prop.put("mail.imap.ssl.enable", "true");
			
			final String username = this.correo;
			final String passw = this.password;
			
			this.sesion = Session.getInstance(prop, new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, passw);
	            }
	        });

			this.store = this.sesion.getStore("imaps");
			this.store.connect();
			
			Runnable runnableWeb = new subServicio(this.correo, this.password, this.banco, this.dataSourceOper, connection, this.store);
			future = exService.submit(runnableWeb);
			
			//future.get(100 , TimeUnit.MINUTES);
	        future.get(100 , TimeUnit.SECONDS); 
		} catch(Exception ex){
			future.cancel(true);
		} finally {			
			try {
				if(connection != null){
					this.store.close();
					this.store = null;
					this.sesion = null;
					
					connection.rollback();
					connection.close();
					connection = null;
					
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Cerrando Conexi�n");
				}
			} catch (SQLException ex) { ex.printStackTrace(); } catch (MessagingException ex) { ex.printStackTrace(); } 
		}
		
		exService.shutdown();
	}
	
	private Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Creando Conexi�n");
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
			
			if(connection.isClosed()){
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
			ex.printStackTrace();
		}
		
		return connection;
	}

	
}
