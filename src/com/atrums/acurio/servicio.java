package com.atrums.acurio;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class servicio {
	
	static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(dateFormat.format(new Date()) + " - " + "Inicio Servicio");
		long time_start, time_end;
		time_start = System.currentTimeMillis();
		
		DataSource dataSource = null;
		
		dataSource = conexion.getDataSource();
		
		try {
			//Correos Adicionales
			
			monitoreoCorre("coopjep@acurioasociados.com","Acurio2017", "COOP. DE AHORRO Y CREDITO JUVENTUD ECUATORIANA PROGRESISTA", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}	
			monitoreoCorre("amazonas@acurioasociados.com","Acurio2017", "BANCO AMAZONAS S.A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {} 
			monitoreoCorre("bolivariano@acurioasociados.com","Acurio2017", "BANCO BOLIVARIANO C.A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {} 
			monitoreoCorre("capital@acurioasociados.com","Acurio2017", "BANCO CAPITAL SOCIEDAD ANONIMA", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("machala@acurioasociados.com","Acurio2017", "BANCO DE MACHALA S A", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("austro@acurioasociados.com","Acurio2017", "BANCO DEL AUSTRO S. A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("pacifico@acurioasociados.com","Acurio2017", "BANCO DEL PACIFICO S.A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			//monitoreoCorre("guayaquil@acurioasociados.com","Acurio2017", "BANCO GUAYAQUIL S.A.", dataSource); //Anterior BG
			monitoreoCorre("avaluosbg@acurioasociados.com","Ecu2020Acurio", "BANCO GUAYAQUIL S.A.", dataSource); //Actual
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			//monitoreoCorre("internacional@acurioasociados.com","Acurio2017", "BANCO INTERNACIONAL S.A", dataSource); //Anterior BI
			monitoreoCorre("avaluosinternacional@acurioasociados.com","Bur36068", "BANCO INTERNACIONAL S.A", dataSource); //Actual
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("procredit@acurioasociados.com","Acurio2017", "BANCO PROCREDIT S.A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("citibank@acurioasociados.com","Acurio2017", "CITIBANK N. A. SUCURSAL ECUADOR", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("claro@acurioasociados.com","Acurio2017", "CLARO", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("particulares@acurioasociados.com","Acurio2017", "CLIENTES PARTICULARES", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("bicsa@acurioasociados.com","Acurio2017", "CLIENTES PARTICULARES II", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("confianza@acurioasociados.com","Acurio2017", "CONFIANZA COMPA��A DE SEGUROS Y REASEGUROS S.A.", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("cfn@acurioasociados.com","Acurio2017", "CORPORACION FINANCIERA NACIONAL", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("finanzaspopulares@acurioasociados.com","Acurio2017", "CORPORACION NACIONAL DE FINANZAS POPULARES Y SOLIDARIAS", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("mutualistapichincha@acurioasociados.com","Acurio2017", "MUTUALISTA PICHINCHA", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("portcoll@acurioasociados.com","Acurio2017", "PORTCOLL S.A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			//monitoreoCorre("produbanco@acurioasociados.com","Acurio2017", "PRODUBANCO", dataSource); //Anterior Prod 
			monitoreoCorre("avaluosprodubanco@acurioasociados.com","Kah61253", "PRODUBANCO", dataSource); //Actual
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("pronaca@acurioasociados.com","Acurio2017", "PRONACA", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("oriente@acurioasociados.com","Acurio2017", "SEGUROS ORIENTE S.A.", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
		    //monitoreoCorre("pichincha@acurioasociados.com","Acurio2017", "BANCO PICHINCHA CA", dataSource); //Anterior BP
			monitoreoCorre("avaluos@acurioasociados.com","Fuv13233", "BANCO PICHINCHA CA", dataSource); //Actual
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("pedidosweb@acurioasociados.com","Acurio2017", "CLIENTES PARTICULARES", dataSource);
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("litoral@acurioasociados.com","Acurio2017", "BANCO DEL LITORAL S.A.", dataSource); //Verificado
			try {Thread.sleep(1000 * 12);} catch (InterruptedException e) {}
			monitoreoCorre("banecuador@acurioasociados.com","Acurio2020", "BANECUADOR B.P.", dataSource); //MAYO 2020

		} catch (Exception ex) {
			// TODO: handle exception
			System.err.println(ex.getStackTrace());
		} finally {
			try {Thread.sleep(120 * 1000);} catch (InterruptedException e) {}
			
			cerrarConexion(dataSource);
		}
		
		time_end = System.currentTimeMillis();
		System.out.println(dateFormat.format(new Date()) + " - " + "the task has taken "+ ( time_end - time_start ) +" milliseconds");
		System.out.println(dateFormat.format(new Date()) + " - " + "Fin Servicio");
	}
	
	public static void monitoreoCorre(String correo, String password, String banco, DataSource dataSourceOper){
		ExecutorService exService = Executors.newFixedThreadPool(1);
		Runnable runnableWeb = new  controlServicio(correo, password, banco, dataSourceOper);
		exService.execute(runnableWeb);
		exService.shutdown();
	}
	
	private static DataSource cerrarConexion(DataSource dataSource){
		try {
			BasicDataSource auxDataSource = (BasicDataSource) dataSource;
			auxDataSource.close();
			auxDataSource = null;
			dataSource = null;
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		
		return dataSource;
	}
}
