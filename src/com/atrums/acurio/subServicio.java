package com.atrums.acurio;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Store;
import javax.sql.DataSource;

public class subServicio implements Runnable{
    String correo = null;
    String password = null;
    String banco = null;
    DataSource dataSourceOper = null;
    
	Store store = null;
    
    Connection connection = null;
    
    static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
    public subServicio(String correo, String password, String banco, DataSource dataSourceOper, Connection connection, Store store){
    	this.correo = correo;
    	this.password = password;
    	this.banco = banco;
    	this.dataSourceOper = dataSourceOper;
    	this.connection = connection;
    	this.store = store;
    }
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Inicio sub-servicio");
		
		try {
			Folder folder = this.store.getFolder("INBOX");
			
			boolean leido = false;
			int intentos = 20;
			
			while (!leido && intentos > 0) {
				try {
					folder.open(Folder.READ_ONLY);
					leido = true;
				} catch (Exception ex) {
					// TODO: handle exception
					if (ex.getMessage().indexOf("Suggested Backoff Time") == -1) {
						System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
						ex.printStackTrace();
						leido = true;
						return;
					} else {
						System.err.println(ex.getMessage());
						intentos--;
						
						if (intentos == 0) {
							return;
						}
					}
				}
			}	
			
			if(this.connection != null){
				Message mensajes[] = folder.getMessages();
				
				System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Correos: " + mensajes.length);
				
				boolean auxPrimero = true;
				Date auxFechaInicial = null;
				
				for(int zi=mensajes.length - 1; (zi >= ((mensajes.length - 101)>0?mensajes.length - 101:0)); zi--){
					long time_start, time_end;
					time_start = System.currentTimeMillis();
					
					Message auxMensaje = mensajes[zi];
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Capturando Mensaje");
					
					String auxContenido = "";
					
					String asunto = auxMensaje.getSubject();

					String fecha_recepcion = new SimpleDateFormat("dd-MM-yyyy").format(auxMensaje.getReceivedDate());
					
					Address[] correo_remitente = auxMensaje.getFrom();
					
					String correoRemi = null;
					
					for(int z=0;z<correo_remitente.length;z++){
						if(correoRemi == null){
							correoRemi = correo_remitente[z].toString();
						}else{
							correoRemi = correoRemi + ", " + correo_remitente[z].toString();
						}
					}
					
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Capturando asunto y remitente: " + asunto + " - " + correoRemi);
					
					Address[] correo_destinatario = auxMensaje.getAllRecipients();
					
					String correoDesti = null;
					
					for(int z=0;z<correo_destinatario.length;z++){
						if(correoDesti == null){
							correoDesti = correo_destinatario[z].toString();
						}else{
							correoDesti = correoDesti + ", " + correo_destinatario[z].toString();
						}
					}
					
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Capturando destinatario: " + correoDesti);
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Mensaje Nro.: " + zi);
					
					try {
						if (auxMensaje.isMimeType("text/*")){
							if(auxMensaje.isMimeType("text/plain")){
								auxContenido = (String) auxMensaje.getContent();
							}
						}else if (auxMensaje.isMimeType("multipart/*")){
							auxContenido = getContenido((Multipart) auxMensaje.getContent());
						}
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
						ex.printStackTrace();
					}
					
					auxContenido = auxContenido.
							replaceAll("'", "").
							replaceAll("&", "").
							replaceAll("%", "").
							replaceAll("�", "").
							replaceAll("!", "");
					
					correoRemi = correoRemi.
							replaceAll("'", "").
							replaceAll("&", "").
							replaceAll("%", "").
							replaceAll("�", "").
							replaceAll("!", "");
					
					correoDesti = correoDesti.
							replaceAll("'", "").
							replaceAll("&", "").
							replaceAll("%", "").
							replaceAll("�", "").
							replaceAll("!", "");
					
					if(auxContenido.equals("")){
						this.connection.rollback();
					}else if(guardarMensaje(this.connection, auxContenido, banco, asunto, fecha_recepcion, correoRemi, correoDesti)){
						this.connection.commit();
					}else{
						this.connection.rollback();
					}
					
					time_end = System.currentTimeMillis();
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "the sub-task has taken "+ ( time_end - time_start ) +" milliseconds");
					
					Date auxFechaFin = auxMensaje.getReceivedDate();
					
					System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Fecha Correo: " + auxFechaFin);
					
					Calendar c = Calendar.getInstance(); 
					c.setTime(auxFechaFin); 
					c.add(Calendar.DATE, 5);
					auxFechaFin = c.getTime();
					
					if (auxPrimero) {
						auxFechaInicial = auxMensaje.getReceivedDate();
						auxPrimero = false;
					} else {
						System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Fechas Correos: " + auxFechaInicial + " - " + auxFechaFin);
						
						if (auxFechaFin.before(auxFechaInicial)) {
							break;
						}
					}
				}
				
				mensajes = null;
				folder = null;
				System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Fin sub-servicio");
			}
		} catch (MessagingException ex) {
			// TODO Auto-generated catch block
			System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
			ex.printStackTrace();
			System.err.println(ex.getMessage());
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
			ex.printStackTrace();
		}
	}
	
	private boolean guardarMensaje(Connection connection, 
			String mensaje, 
			String banco, 
			String asunto, 
			String fecha_recepcion, 
			String correoRemi, 
			String correoDesti){
		Statement statement = null;
		String sql;
		int registro = 0;
		int total = 0;
		String sucursal = null;
		String region = null;
		
		if(correoDesti.toUpperCase().indexOf("avaluos@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "QUITO";
			region = "PICHINCHA";
		}else if(correoDesti.toUpperCase().indexOf("avaluosuio@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "QUITO";
			region = "PICHINCHA";
		}else if(correoDesti.toUpperCase().indexOf("avaluosgye@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "GUAYAQUIL";
			region = "GUAYAS";
		}else if(correoDesti.toUpperCase().indexOf("avaluosguayaquil@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "GUAYAQUIL";
			region = "GUAYAS";
		}else if(correoDesti.toUpperCase().indexOf("avaluosmanta@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "MANTA";
			region = "MANABI";
		}else if(correoDesti.toUpperCase().indexOf("avaluosbg@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "GUAYAQUIL";
			region = "GUAYAS";
		}else if(correoDesti.toUpperCase().indexOf("avaluosamb@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "AMBATO";
			region = "TUNGURAHUA";
		}else if(correoDesti.toUpperCase().indexOf("avaluoscue@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "CUENCA";
			region = "AZUAY";
		}else if(correoDesti.toUpperCase().indexOf("avaluosmac@acurioasociados.com".toUpperCase()) != -1){
			sucursal = "MACHALA";
			region = "EL ORO";
		}

		if (sucursal == null) {
			return false;
		}
		
		try {
			statement = connection.createStatement();
			
			ResultSet rs = null;
			
			sql = "SELECT count(*) AS TOTAL FROM avb_solicitud "
					+ "WHERE (asunto LIKE '" + asunto + "' AND correo_remitente LIKE '" + correoRemi + "') "
					+ "AND c_bpartner_id IN (SELECT c_bpartner_id FROM c_bpartner "
					+ "WHERE em_atacv_entidad = 'Y' AND upper(name) LIKE '" + banco + "') "
					+ "AND fecha_recepcion = date('" + fecha_recepcion + "');";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				if(rs.getString("total") != null){
					total = rs.getInt("total");
				}
			}
			
			rs.close();
			rs = null;
			
			if(total == 0){

				sql = "INSERT INTO avb_solicitud("
						+ "avb_solicitud_id, ad_client_id, ad_org_id, isactive, created, "
						+ "createdby, updated, updatedby, c_bpartner_id, c_bpartner_location_id, "
						+ "ad_user_id, solicitud, docstatus, docaction_sol, documento, atacv_avaluo_id, "
						+ "c_bpartner_name, ad_org2_id, fecha_inspeccion, c_bpartner2_id, "
						+ "fecha_pedido, novedades, valor_factura, mensaje_guardar, c_country_id, "
						+ "c_region_id, atacv_novedad_id, evento, docstatus_banco, atacv_nov_banco_id, "
						+ "fecha_liberacion, asunto, fecha_recepcion, correo_remitente, correo_destinatario) "
						+ "VALUES (get_uuid(), '7E16CAABBC3A42999A1C6AC1A79A482B', '0', 'Y', now(), "
						+ "'100', now(), '100', (SELECT c_bpartner_id FROM c_bpartner WHERE em_atacv_entidad = 'Y' AND ad_client_id='7E16CAABBC3A42999A1C6AC1A79A482B' AND upper(name) LIKE '" + banco + "'), null, "
						+ "null, '" + mensaje + "', 'BO', 'EN', (SELECT to_number(MAX(documento)) + 1 FROM avb_solicitud where ad_client_id='7E16CAABBC3A42999A1C6AC1A79A482B'), null, "
						+ "null, (SELECT ad_org_id FROM ad_org WHERE upper(name) LIKE '" + sucursal + "' and ad_client_id='7E16CAABBC3A42999A1C6AC1A79A482B' LIMIT 1), null, null, "
						+ "now(), null, null, null, '171', "
						+ "(SELECT cr.c_region_id FROM c_region cr WHERE upper(cr.name) LIKE '" + region + "'), null, null, null, null, "
						+ "null, '" + asunto + "', date('" + fecha_recepcion + "'), '" + correoRemi + "','" + correoDesti + "');";
				
				registro = statement.executeUpdate(sql);
			}else{
				sql = "SELECT avb_solicitud_id "
						+ "FROM avb_solicitud "
						+ "WHERE solicitud = '" + mensaje + "' " 
						+ "AND c_bpartner_id IN (SELECT c_bpartner_id FROM c_bpartner WHERE em_atacv_entidad = 'Y' and ad_client_id='7E16CAABBC3A42999A1C6AC1A79A482B' AND upper(name) LIKE '" + banco + "') " 
						+ "AND asunto IS NULL;";
				
				rs = statement.executeQuery(sql);
				
				String auxSoliId = null;
				
				while (rs.next()){
					auxSoliId = rs.getString("avb_solicitud_id");
				}
				
				rs.close();
				rs = null;
				
				if(auxSoliId != null){
					sql = "UPDATE avb_solicitud SET asunto = '" + asunto + "', "
							+ "fecha_recepcion = date('" + fecha_recepcion + "'), "
							+ "correo_remitente = '" + correoRemi + "', "
							+ "correo_destinatario = '" + correoDesti + "' "
							+ "WHERE avb_solicitud_id = '" + auxSoliId + "'";
					
					registro = statement.executeUpdate(sql);
				}
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
			ex.printStackTrace();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			System.err.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - " + "Error");
			ex.printStackTrace();
		}  finally {
			try { 
				if (statement != null){
					statement.close();
					statement = null;
					sql = null;
				} 
			} catch (Exception ex) {
				ex.printStackTrace();
			};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	private String getContenido(Multipart multi) throws MessagingException, IOException {
		String result = "";
		
		System.out.println(dateFormat.format(new Date()) + " - (" + this.correo + ") - partes: " + multi.getCount());
		
		for(int i = 0; i<multi.getCount(); i++){
			if (multi.getBodyPart(i).isMimeType("text/*")){
				if(multi.getBodyPart(i).isMimeType("text/plain")){
					result = result + "\n" + (String) multi.getBodyPart(i).getContent();
					break;
				} else if (multi.getBodyPart(i).isMimeType("text/html")) {
		            result = result + "\n" + org.jsoup.Jsoup.parse((String) multi.getBodyPart(i).getContent()).text();
		            break;
		        }
			}else if(multi.getBodyPart(i).isMimeType("multipart/*")){
				result = result + getContenido((Multipart) multi.getBodyPart(i).getContent());
			}
		}
		
		return result;
	}
}
